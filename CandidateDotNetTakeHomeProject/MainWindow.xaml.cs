﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Data.SQLite;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

/*
    How could you solve being able to load in any data source into this interface?
    What should be taken into consideration when declaring private and public parts of code?
    What is garbage collection?
    Add a column to the data grid
    Increase the window size to 800 x 600
    Add the ability to edit data in the grid and save it to the database
    Add the ability for the user to load any sqlite file they choose giving they supply the correct tables and type of data
    How might you improve upon this projects code and/or setup?
*/

/*
    Bugs:
        1. Users are reporting that there is an error that occurs when closing the application. Please find and correct the bug.
        2. Users are reporting that the data is missing from the grid. Please find out why this is happening and fix it.
  
*/

namespace CandidateDotNetTakeHomeProject {
    public class DataSourceLoadedEventArgs : EventArgs {
        public List<Object> data;
    }

    public partial class MainWindow : Window {
        //      Data loaded event
        private delegate void DataSourceLoadedEventHandler(object sender, DataSourceLoadedEventArgs e);
        private event DataSourceLoadedEventHandler OnDataSourceLoaded;

        //      Connection to the sqlite datasource
        private SQLiteConnection dataSource = null;

        public MainWindow() {
            //      Function call that is need by wpf
            InitializeComponent();

            //      Handle the closing event
            this.Closing += this.ApplicationClosing;

            //      Handle the data loaded event
            this.OnDataSourceLoaded += this.HandleDataSourceLoaded;

            //      Initialize a connection to the data source
            this.InitializeDataSource();

            //      Read in the data from the datasource
            //      Then load it into the UI
            //      Do this asynchronously
            Task.Factory.StartNew(() => {
                //      Raise an event that our data was loaded
                OnDataSourceLoaded(this, new DataSourceLoadedEventArgs() { data = this.DeserializeData(this.getDataSourceData()) });
            });
        }

        //      Function to open a connection to an sqlite data source
        private void InitializeDataSource() {
            //      Executable opens the data.sqlite file which is located at the same folder in the same directory level
            this.dataSource = new SQLiteConnection("Data Source=./data.sqlite;Version=3;PRAGMA synchronous=OFF;PRAGMA cache_size=20000; PRAGMA page_size=32768");
            this.dataSource.Open();

            if (dataSource.State != System.Data.ConnectionState.Open)
                throw new System.Data.SQLite.SQLiteException(SQLiteErrorCode.CantOpen, "Error opening connection to the data source. Please check your connection string!");
        }

        //      Event handler for exit click
        private void ApplicationClosing(object sender, System.ComponentModel.CancelEventArgs e) {
            //      Close the data source
            this.dataSource.Close();
            this.dataSource.Dispose();
        }

        //      Event handler for when the data source is read in and deserialized
        private void HandleDataSourceLoaded(object sender, DataSourceLoadedEventArgs e) {
            Console.WriteLine("Data was loaded and includes " + e.data.Count.ToString() + " rows!");

            this.featuresDataGrid.DataContext = e.data;
        }

        //      Read the data from the sqlite file
        private DataTable getDataSourceData() {
            //      Create a new sqlite command using the already established connection
            var getDataCommand = this.dataSource.CreateCommand();
            getDataCommand.CommandText = "SELECT * FROM resources";

            //      Push the data into a data table
            var dataAdapter = new SQLiteDataAdapter(getDataCommand);
            var featureDataTable = new DataTable();
            dataAdapter.Fill(featureDataTable);
            
            //      Clean up
            dataAdapter.Dispose();
            getDataCommand.Dispose();

            return featureDataTable;
        }

        //      Function that will take in a datatable that was filled from the data source and deserialize it
        //      into its repsective object list
        private List<Object> DeserializeData(DataTable data) {
            var deserializedData = new List<Object>();
            var settings = new JsonSerializerSettings();

            foreach (DataRow row in data.Rows) {
                var feature = JsonConvert.DeserializeObject<Feature>(row["geojson"].ToString());
                feature.rowId = Int32.Parse(row["id"].ToString());
                deserializedData.Add(feature);
            }

            return deserializedData;
        }
    }


    /*
            Class declarations
    */
    public class Feature {
        public int rowId { get; set; }
        public String type { get; set; }
        public FeatureProperties properties { get; set; }
    }

    public class FeatureProperties {
        public String comments { get; set; }
        public String UNIQUEID { get; set; }
        public String WRK_AREA { get; set; }
        public String WRK_REGION { get; set; }
    }
}
